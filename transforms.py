import images


def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    return [[to_change_to if tupla == to_change else tupla for tupla in lista] for lista in image]


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    pixels = [tupla for lista in image for tupla in lista]
    return [[pixels[y * height + x] for y in range(width)] for x in range(height)]


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    size = len(image)
    for num in range(size // 2):
        image[num], image[size - 1 - num] = image[size - 1 - num], image[num]
    return image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    return [
        [
            tuple((x + increment) % 256 for x in tupla)
            for tupla in lista
        ]
        for lista in image
    ]


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    height = len(image)
    width = len(image[0])

    blurred_image = []
    for i in range(height):
        row = []
        for j in range(width):
            total_pixels = 0
            total_red = 0
            total_green = 0
            total_blue = 0

            for m in range(-1, 2):
                for n in range(-1, 2):
                    if 0 <= i + m < height and 0 <= j + n < width:
                        pixel = image[i + m][j + n]
                        total_red += pixel[0]
                        total_green += pixel[1]
                        total_blue += pixel[2]
                        total_pixels += 1

            avg_red = total_red // total_pixels
            avg_green = total_green // total_pixels
            avg_blue = total_blue // total_pixels

            blurred_pixel = (avg_red, avg_green, avg_blue)
            row.append(blurred_pixel)

        blurred_image.append(row)

    return blurred_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    width = len(image[0])

    # Desplazamiento vertical
    if vertical > 0:
        image = [[(0, 0, 0) for _ in range(width)] for _ in range(vertical)] + image[:-vertical]
    elif vertical < 0:
        image = image[-vertical:] + [[(0, 0, 0) for _ in range(width)] for _ in range(-vertical)]

    # Desplazamiento horizontal
    if horizontal > 0:
        image = [[(0, 0, 0) for _ in range(horizontal)] + row[:-horizontal] for row in image]
    elif horizontal < 0:
        image = [row[-horizontal:] + [(0, 0, 0) for _ in range(-horizontal)] for row in image]

    return image


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    return [lista[y:y + height] for lista in image[x:x + width]]


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    return [
        [
            ((tupla[0] + tupla[1] + tupla[2]) // 3,) * 3
            for tupla in lista
        ]
        for lista in image
    ]


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    return [
        [
            tuple(int(x * r) % 256 for x in tupla)
            for tupla in lista
        ]
        for lista in image
    ]


def sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    sepia_image = []
    for lista in image:
        sepia_list = []
        for tupla in lista:
            r, g, b = tupla
            new_r = int(0.393 * r + 0.769 * g + 0.189 * b)
            new_g = int(0.349 * r + 0.686 * g + 0.168 * b)
            new_b = int(0.272 * r + 0.534 * g + 0.131 * b)
            sepia_list.append((min(255, new_r), min(255, new_g), min(255, new_b)))
        sepia_image.append(sepia_list)
    return sepia_image


def adjust_brightness(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:
    adjusted_image = []
    for lista in image:
        adjusted_list = []
        for tupla in lista:
            adjusted_tupla = tuple(int(max(0, min(255, channel * factor))) for channel in tupla)
            adjusted_list.append(adjusted_tupla)
        adjusted_image.append(adjusted_list)
    return adjusted_image


def adjust_contrast(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:
    adjusted_image = []
    for lista in image:
        adjusted_list = []
        for tupla in lista:
            adjusted_tupla = tuple(int(max(0, min(255, (channel - 128) * factor + 128))) for channel in tupla)
            adjusted_list.append(adjusted_tupla)
        adjusted_image.append(adjusted_list)
    return adjusted_image
