import sys
import images
import transforms


def main():
    print()
    print("Al estar utilizando transform_args en lugar de transform_simple o transform_multi,"
          "este programa solo realiza una función y el resto no serán ejecutadas.")
    try:
        nombre, *args = sys.argv[1:]
    except ValueError:
        print("Error.... El formato de entrada no es válido.")
        sys.exit(1)

    try:
        algo = nombre.split(".jpg")
        algo = nombre.split(".jpeg")
        algo = nombre.split(".png")
    except ValueError:
        print("Error.... El archivo que has introducido no tiene extensión .jpg")
        sys.exit(1)

    image = images.read_img(nombre)
    opcion = sys.argv[2]

    if opcion == "change_colors":
        try:
            tup1 = tuple(map(int, sys.argv[3:6]))
            tup2 = tuple(map(int, sys.argv[6:9]))
        except (ValueError, IndexError):
            print("Error... Alguno de los valores que has introducido no es correcto o no es un número entero.")
            sys.exit(1)

        image = transforms.change_colors(image, tup1, tup2)

    elif opcion == "rotate_right":
        image = transforms.rotate_right(image)

    elif opcion == "mirror":
        image = transforms.mirror(image)

    elif opcion == "rotate_colors":
        try:
            incremento = int(sys.argv[3])
        except (ValueError, IndexError):
            print("Error... Debes introducir un número para el incremento de colores.")
            sys.exit(1)

        image = transforms.rotate_colors(image, incremento)

    elif opcion == "blur":
        image = transforms.blur(image)

    elif opcion == "filter":
        try:
            num1, num2, num3 = map(float, sys.argv[3:6])
        except (ValueError, IndexError):
            print("Error... Alguno de los valores que has introducido no es un número o tiene decimales.")
            sys.exit(1)

        image = transforms.filter(image, num1, num2, num3)

    elif opcion == "crop":
        try:
            value_x, value_y, ancho, alto = map(int, sys.argv[3:7])
        except (ValueError, IndexError):
            print("Error... Faltan valores para recortar la image. Debes introducir ancho y alto.")
            sys.exit(1)

        image = transforms.crop(image, value_x, value_y, ancho, alto)

    elif opcion == "shift":
        try:
            horizontal, vertical = map(int, sys.argv[3:5])
        except (ValueError, IndexError):
            print("Error... Debes introducir valores enteros para el desplazamiento de la image.")
            sys.exit(1)

        image = transforms.shift(image, horizontal, vertical)

    elif opcion == "grayscale":
        image = transforms.grayscale(image)

    elif opcion == "sepia":
        image = transforms.sepia(image)

    elif opcion == "adjust_brightness":
        try:
            factor = float(sys.argv[3])
        except IndexError:
            print("Error... Debes introducir un número para el factor de luminosidad.")
            sys.exit(1)
        image = transforms.adjust_brightness(image, factor)

    elif opcion == "adjust_contrast":
        try:
            factor = float(sys.argv[3])
        except IndexError:
            print("Error... Debes introducir un número para el factor de contraste.")
            sys.exit(1)
        image = transforms.adjust_contrast(image, factor)

    else:
        print("Error... El nombre de la función que has introducido es incorrecto. Inténtalo de nuevo.")
        sys.exit(1)

    images.write_img(image, f"{algo[0]}_trans.jpg")
    print(f"Imágen modificada y descargada como {algo[0]}_trans.jpg en su mismo directorio.")


if __name__ == '__main__':
    main()
