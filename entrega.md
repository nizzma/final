# ENTREGA CONVOCATORIA ENERO
Nicolás Ibáñez Zurdo n.ibanez.2023@alumnos.urjc.es

https://youtu.be/b7gumMJX0y4

Requisitos mínimos
-Métodos: change_colors rotate_right mirror rotate_colors blur(puede no llegar a apreciarse en el vídeo) shift crop
-transform_simple.py
-trasnsform_args.py
-transform_multi.py

Requisitos opcionales
-Métodos: sepia adjust_brightness adjust_contrast

*El vídeo no muestra todas las funciones para no extenderlo demás, algunas las he omitido por gran similitud en el código.