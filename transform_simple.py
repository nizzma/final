import sys
import images
import transforms


def main():
    print()
    print("Al estar utilizando transform_simple en lugar de transform_args o transform_multi,"
          "este programa solo realiza una función que no requiera de parámetros adicionales.")
    try:
        nombre, *args = sys.argv[1:]
    except ValueError:
        print("Error.... El formato de entrada no es válido.")
        sys.exit(1)

    try:
        algo = nombre.split(".jpg")
        algo = nombre.split(".jpeg")
        algo = nombre.split(".png")
    except ValueError:
        print("Error.... El archivo que has introducido no tiene extensión .jpg")
        sys.exit(1)

    image = images.read_img(nombre)
    opcion = sys.argv[2]

    if opcion == "rotate_right":
        imagen = transforms.rotate_right(image)

    elif opcion == "mirror":
        imagen = transforms.mirror(image)

    elif opcion == "blur":
        imagen = transforms.blur(image)

    elif opcion == "grayscale":
        imagen = transforms.grayscale(image)

    elif opcion == "sepia":
        imagen = transforms.sepia(image)

    else:
        print("Error... El nombre de la función que has introducido es incorrecto. Inténtalo de nuevo.")
        sys.exit(1)

    images.write_img(imagen, f"{algo[0]}_trans.jpg")
    print(f"Imágen modificada y descargada como {algo[0]}_trans.jpg en su mismo directorio.")


if __name__ == '__main__':
    main()
